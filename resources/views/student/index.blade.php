@extends('master')

@section('content')

  @if(session('message'))
    <div class="alert alert-success" id="message">
      <p><i class="fa fa-check"></i></i>&nbsp;{{session('message')}}</p>
    </div>
  @endif
  <table class="table table-striped table-hover table-bordered" style="margin-top:30px">
    @if(count($students) > 0)
    <tr>
      <th>Name</th>
      <th>Actions</th>
    </tr>
    @foreach($students as $student)
      <tr>
        <td>{{ucwords($student->name)}}</td>
        <td><a href="<?php echo route('student.edit', array('id' => $student->id)) ?>" class="button-clear"><span><i class="fa fa-pencil"></i>&nbsp; Edit</span></a> &nbsp; &nbsp; 
        	<a href="javascript:void(0);" onclick="deleteRecord('<?php echo route('student.delete', array('id' => $student->id)) ?>')" class="button"><span><i class="fa fa-trash-o"></i>&nbsp; Delete</span></a>
        </td>
      </tr>
    @endforeach

    @else
      <tr>
        <td>No records to show.</td>
      </tr>
    @endif
  </table>

<div style="text-align:center">
{!! $students->render() !!}
</div>
  <a href="<?php echo route('student.add') ?>" class="button"><i class="fa fa-plus"></i>&nbsp; Add Student</a>

@stop

