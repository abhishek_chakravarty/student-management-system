@extends('master')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
      <h4>Updation failed because:</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <h4>Edit Student Details</h4>
  <form style="margin-top:30px" method="POST" action="<?php echo route('student.update') ?>">
  	<?php echo csrf_field(); ?>
    <input type="hidden" id="url" value="<?php echo route('student.checkEmail') ?>"> 
    <input type="hidden" name="id" id="id" value="{{$student->id}}"> 
  	<div class="form-group">
      <label for="name">Name: </label><input type="text" name="name" value="{{$student->name}}" class="form-control" id="name" required>
    </div>
    <div class="form-group">
      <label for="email">Name: </label><input type="email" name="email" value="{{$student->email}}" class="form-control" id="email" required>
      <p id="exist" style="color:red; display:none">*Email already exist! Try another one.<p>
    </div>
    <div class="form-group">
      <label for="address">Address: </label><textarea id="address" name="address" class="form-control" required>{{$student->address}}</textarea>
	</div>
  <br>
  Gender: &nbsp;
	<div class="radio">
      <label for="male"> <input type="radio" id="male" name="gender" value="male" <?php if($student->gender == 'male') echo "checked"; ?> required > Male</label><br>
      <label for="female"> <input type="radio" id="female" name="gender" value="female" <?php if($student->gender == 'female') echo "checked"; ?> > Female</label>
    </div>
    <div class="form-group">
      <label for="year_of_pass">Expected year of passing:</label>
      <select id="year_of_pass" name="year_of_pass" class="form-control" required>
      	<?php $year_of_pass = $student->year_of_pass ?>
        <option value="2014" <?php if($year_of_pass == 2014) echo 'selected="selected"' ?> >2014</option>
        <option value="2015" <?php if($year_of_pass == 2015) echo 'selected="selected"' ?> >2015</option>
        <option value="2016" <?php if($year_of_pass == 2016) echo 'selected="selected"' ?> >2016</option>
        <option value="2017" <?php if($year_of_pass == 2017) echo 'selected="selected"' ?> >2017</option>
        <option value="2018" <?php if($year_of_pass == 2018) echo 'selected="selected"' ?> >2018</option>
      </select>
    </div>
    <div class="form-group">
      <h4>Interests:<h4>
      <?php 
        foreach($student->interests as $interest){
          $interests_array[] = $interest->id;
        }
      ?>
      @foreach($interests as $interest)
      <label class="checkbox-inline">
        <input type="checkbox" name="interests[]" value="{{$interest->id}}" <?php if(in_array($interest->id, $interests_array)) echo 'checked' ?> >{{ucfirst($interest->name)}}
      </label>
      @endforeach
    </div>
    <br><br><br>
    <a href="<?php echo route('student.index') ?>" class="button-clear"><span>Cancel</span></a>&nbsp;&nbsp;
    <input type="submit" value="Update" class="button">
  </form>

@stop
