@extends('master')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
      <h4>Failed because:</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h4>Add Student</h4>
  <form style="margin-top:30px" method="POST" action="<?php echo route('student.store') ?>">
  	<?php echo csrf_field(); ?>
    <input type="hidden" id="url" value="<?php echo route('student.checkEmail') ?>">
  	<div class="form-group">
      <label for="name">Name: </label><input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" required>
    </div>
    <div class="form-group">
      <label for="email">Email: </label><input type="email" name="email" value="{{old('email')}}" class="form-control" id="email" required>
      <p id="exist" style="color:red; display:none">*Email already exist! Try another one.<p>
    </div>
    <div class="form-group">
      <label for="address">Address: </label><textarea id="address" name="address" class="form-control">{{old('address')}}</textarea>
	</div>
	Gender: &nbsp;
  <div class="radio">
      <label for="male"> <input type="radio" id="male" name="gender" value="male" @if(old('gender') == 'male') checked @endif required > Male</label><br>
      <label for="female"> <input type="radio" id="female" name="gender" value="female" @if(old('gender') == 'female') checked @endif > Female</label>
    </div>
    <div class="form-group">
      <label for="year_of_pass">Expected year of passing:</label>
      <select id="year_of_pass" name="year_of_pass" class="form-control" required>
        <option value="2014" @if(old('year_of_pass') == '2014') selected @endif>2014</option>
        <option value="2015" @if(old('year_of_pass') == '2015') selected @endif>2015</option>
        <option value="2016" @if(old('year_of_pass') == '2016') selected @endif>2016</option>
        <option value="2017" @if(old('year_of_pass') == '2017') selected @endif>2017</option>
        <option value="2018" @if(old('year_of_pass') == '2018') selected @endif>2018</option>
      </select>
    </div>
    <div class="form-group">
      <h4>Interests:<h4>
      @foreach($interests as $interest)
      <label class="checkbox-inline">
        <input type="checkbox" name="interests[]" value="{{$interest->id}}" @if(old('interests') != null) @if(in_array($interest->id, old('interests'))) checked @endif @endif >{{ucfirst($interest->name)}}
      </label>
      @endforeach
    </div>
    <br><br><br>
    <a href="<?php echo route('student.index') ?>" class="button-clear"><span>Cancel</span></a>&nbsp;&nbsp;
    <input type="submit" value="Add" class="button">
  </form>
  @stop


