<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model{

    //no need for timestamps
    public $timestamps = false;

    public function interests(){
      return $this->hasMany('App\Student', 'id');
    }

    //get all students from student table
    public static function get(){
      $interests = Interest::all();
      return $interests;
    }
}