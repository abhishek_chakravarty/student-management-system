<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Student extends Model{
    
    //prevent mass assignment of interest in students table
    protected $guarded = ['interests'];

    public function rules($id=0){
    	return $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:students,id,'.($id ? "$id" : ''),
        'address' => 'required',
        'gender' => 'required',
        'year_of_pass' => 'required',
        'interests' => 'required'
      ];
    }

    //Many to Many relationship with Interests table
    public function interests(){
      return $this->belongsToMany('App\Interest');
    }

    //get all students from student table
    public static function get(){
      $students = Student::Paginate(5);
      return $students;
    }

    //creates new student
    public function add($inputs){

      $validation = Validator::make($inputs, $this->rules());
      if($validation->passes()){
        $student = Student::create($inputs);
        dd($inputs['interests']);
        $student->interests()->sync($inputs['interests']);
        return ['success' => true, 'data' => 'student added successfuly', 'errors' => null];
      }
      else{
      	return ['success' => false, 'data' => $inputs, 'errors' => $validation->errors()];
      }
    }

    //deletes student
    public function remove()
  	{
  		$this->interests()->detach();
  		$this->delete();
  	}

    //update student record
	public function edit($request){
      $id = $request->id;
      $validation = Validator::make($request->all(), $this->rules($id));
      if($validation->passes()){
	    $input = $request->all();
	    $student = $this->fill($input)->save();
	    $this->interests()->sync($request->input('interests'));
	  }
	  else{
      	return $validation;
      }

	}
}
