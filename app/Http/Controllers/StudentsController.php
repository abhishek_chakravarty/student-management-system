<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Student;
use App\Interest;

class StudentsController extends Controller{

  

  public function index(){
    $students = Student::get();
    return view('student.index', compact('students'));
  }

  public function add(){
    $interests = Interest::get();
    return view('student.add', compact('interests'));
  }

  public function store(Request $request){ 

    $student = new Student;
    $valid_status = $student->add($request);

    if($valid_status){
      return redirect()->back()->with('errors', $valid_status->messages())->withInput();
    }
    else{
      $message = "New student ".$request->get('name')." added.";
      return redirect('/')->with('message', $message);
    }
  }

  public function delete($id){
    $student = Student::find($id);
    $studentName = $student->name; //get the student name to show in view
    $student->remove();
    return redirect('/')->with('message', 'Student '.$studentName.' deleted!!');
  }

  public function edit($id){
    $student = Student::find($id);
    $interests = Interest::all();
    return view('student.edit', compact('student', 'interests'));
  }

  public function update(Request $request){ 
    $id = $request->id;
    $student = Student::find($id);
    $valid_status = $student->edit($request);

    if($valid_status){
      return redirect()->back()->with('errors', $valid_status->messages())->withInput();
    }
    else{
      $message = "Edit successful";
      return redirect('/')->with('message', $message);
    }
  }

  public function checkEmail(Request $request){ 
    $id = $request->get('id');
    //if request coming from edit page
    if($id){
      $student = Student::find($id);
      if($student->email == $request->get('email'))
        return 0;  //-> means user's own email
    }
    //if it's not user's original email or request coming from add page
    if (Student::where('email', $request->email)->count() > 0) {
      return 1;
    }
    else
      return 0;
  }
}
