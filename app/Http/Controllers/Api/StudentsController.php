<?php

namespace App\Http\Controllers\Api;

//use Illuminate\Http\Request;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Student;
use App\Interest;
use Log;

class StudentsController extends Controller{

  public function __construct() {
    Log::info(request()->url()." ".json_encode(request()->all()));
  }

  public function index()
  {
    $students = Student::with('interests')->get();

    $response = ['success' => true, 'data' => $students->toArray(), 'errors' => null];

    return response()->json($response);
  }

  public function show($id)
  {
    $student = Student::find($id);

    $interests = $student->interests;

    $data = ['student_details' => $student];

    $response = ['success' => true, 'data' => $data, 'errors' => null];

    return response()->json($response);
  }

  public function store()
  {
    $inputs = request()->all();

    $student = new Student;

    $response = $student->add($inputs);

    return response()->json($response);
  }
}
