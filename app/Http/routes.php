<?php

Route::group(['namespace' => 'Api', 'prefix' => 'api'], function(){
	Route::get('/', array('as' => 'student.index', 'uses' => 'StudentsController@index'));

	Route::get('add', array('as' => 'student.add', 'uses' => 'StudentsController@add'));

	Route::get('show/{id}', array('as' => 'student.show', 'uses' => 'StudentsController@show'));

	Route::post('store', array('as' => 'student.store', 'uses' => 'StudentsController@store'));

	Route::get('delete/{id}', array('as' => 'student.delete', 'uses' => 'StudentsController@delete'));

	Route::get('edit/{id}', array('as' => 'student.edit', 'uses' => 'StudentsController@edit'));

	Route::post('update', array('as' => 'student.update', 'uses' => 'StudentsController@update'));

	Route::post('checkEmail', array('as' => 'student.checkEmail', 'uses' => 'StudentsController@checkEmail'));
});

Route::get('/', array('as' => 'student.index', 'uses' => 'StudentsController@index'));

Route::get('add', array('as' => 'student.add', 'uses' => 'StudentsController@add'));

Route::post('store', array('as' => 'student.store', 'uses' => 'StudentsController@store'));

Route::get('delete/{id}', array('as' => 'student.delete', 'uses' => 'StudentsController@delete'));

Route::get('edit/{id}', array('as' => 'student.edit', 'uses' => 'StudentsController@edit'));

Route::post('update', array('as' => 'student.update', 'uses' => 'StudentsController@update'));

Route::post('checkEmail', array('as' => 'student.checkEmail', 'uses' => 'StudentsController@checkEmail'));

