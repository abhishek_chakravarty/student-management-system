$(document).ready(function(){
  $("#email").focusout(function() {
    $('input[type="submit"]').prop('disabled', true);
  });
  
  $("#email").focusout(function() {
        var email  = $("#email").val();
        var id     = $('#id').val();
        var token  = $('input[name=_token]').val();
        var url    = $("#url").val();
        $.ajax({
                type: "POST",
                url : url,
                data : {'email': email, 'id': id, '_token': token},
                success : function(data){
                  if(data == 1){
                    $("#exist").show('slow');
                    $('input[type="submit"]').prop('disabled', true);
                  }
                  if(data == 0){
                    $("#exist").hide('slow');
                    $('input[type="submit"]').prop('disabled', false);
                  }
                }       
        });
    });

});

function deleteRecord(url){
    var sure = confirm("Are you sure you want to delete this record?");

    if(sure){
      window.location = url;
    }
    else{
      return false;
  }
  }